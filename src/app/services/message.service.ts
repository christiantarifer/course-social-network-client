import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// **************************************************************** //

// * API URL
import { Global } from './global';

// **************************************************************** //

import { Message } from '../models/message';

// **************************************************************** //

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public url: string;

  constructor( private _http: HttpClient ) {

    this.url = Global.url;

  }

  addMessage( token, message ): Observable<any> {

    const params = JSON.stringify(message);

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                   .set('Authorization', token);

    return this._http.post(`${this.url}message`, params, { headers });

  }

  getMyMessages(token, page = 1): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                   .set('Authorization', token);


    return this._http.get(`${this.url}my-messages/${page}`, { headers });
  }

  getEmitMessages(token, page = 1): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                   .set('Authorization', token);


    return this._http.get(`${this.url}messages/${page}`, { headers });
  }


}
