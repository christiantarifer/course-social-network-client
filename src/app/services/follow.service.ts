import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// * API URL
import { Global } from './global';

// * MODELS
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  public url: string;

  constructor(
    private _http: HttpClient
  ) {

    this.url = Global.url;

  }

  addFollow(token, follow): Observable<any>  {

    const params = JSON.stringify( follow );

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                    .set('Authorization', token);

    return this._http.post(`${this.url}follow`, params, { headers });

  }

  deleteFollow(token, id): Observable<any> {

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                    .set('Authorization', token);

    return this._http.delete( `${this.url}follow/${id}`, { headers} );

  }

  getFollowing(token, userId: string = null, page = 1 ): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                     .set('Authorization', token);

    let followingUrl = `${this.url}following`;

    if ( userId !=  null ) {

      followingUrl = `${this.url}following/${userId}/${page}`;

    }

    return this._http.get( followingUrl , { headers} );

  }

  getFollowed(token, userId: string = null, page = 1 ): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                     .set('Authorization', token);

    let followedUrl = `${this.url}followed`;

    if ( userId !=  null ) {

      followedUrl = `${this.url}followed/${userId}/${page}`;

    }

    return this._http.get( followedUrl , { headers} );

  }

  getMyFollows(token): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                     .set('Authorization', token);

    return this._http.get(`${this.url}get-my-follows/true`, { headers } )

  }

}
