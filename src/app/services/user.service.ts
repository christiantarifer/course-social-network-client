import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// * API URL
import { Global } from './global';

// * MODELS
import { User } from '../models/user';




@Injectable({
  providedIn: 'root'
})
export class UserService {

  public url: string;
  public identity;
  public token;
  public stats;

  constructor( private _http: HttpClient) {

    this.url = Global.url;

  }

  // * ------------------------------------------------------------------------------------------------ * //

  register(user: User): Observable<any> {

    const params = JSON.stringify(user);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._http.post(`${this.url}register`, params, {headers});

  }

  // * ------------------------------------------------------------------------------------------------ * //

  signup(user, gettoken = null): Observable<any>{

    if ( gettoken != null ){

      user.gettoken = gettoken;

    }

    const params = JSON.stringify(user);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._http.post(`${this.url}login`, params, { headers });

  }

  // * ------------------------------------------------------------------------------------------------ * //

  getIdentity(): User {

    const identity: User = JSON.parse(localStorage.getItem('identity'));

    if ( identity !== undefined ){

      this.identity = identity;

    } else {

      this.identity = null;

    }

    return identity;

  }

  // * ------------------------------------------------------------------------------------------------ * //

  getToken() {

    const token = JSON.parse(localStorage.getItem('token'));

    if ( token !== 'undefined' ) {

      this.token = token;

    } else {

      this.token = null;

    }

    return this.token;
  }

  // * ------------------------------------------------------------------------------------------------ * //

  getStats(): string {

    const stats = JSON.parse( localStorage.getItem('stats') );

    if ( stats !== 'undefined' ) {

      this.stats = stats;

    } else {

      this.stats = null;

    }

    return this.stats;

  }

  // * ------------------------------------------------------------------------------------------------ * //

  getCounters(userId = null): Observable<any>{

    const headers = new HttpHeaders().set('Content-Type', 'application/json')
                                   .set('Authorization', this.getToken() );


    if ( userId != null ){

      return this._http.get( `${this.url}counters/${userId}`, {headers});

    } else {

      return this._http.get( `${this.url}counters`, {headers});
    }



  }

  // * ------------------------------------------------------------------------------------------------ * //

  updateUser(user: User): Observable<any> {

    const params = JSON.stringify( user );

    const headers = new HttpHeaders().set( 'Content-Type', 'application/json' )
                                   .set('Authorization', this.getToken() );


    return this._http.put(`${this.url}update-user/${user._id}`, params, { headers });

  }

  // * ------------------------------------------------------------------------------------------------ * //

  getUsers(page = null): Observable<any>{

    const headers = new HttpHeaders().set( 'Content-Type', 'application/json' )
                                    .set( 'Authorization', this.getToken() );

    return this._http.get(`${this.url}users/${page}`, { headers });

  }

  // * ------------------------------------------------------------------------------------------------ * //

  getUser( id ): Observable<any>{

    const headers = new HttpHeaders().set( 'Content-Type', 'application/json' )
                                    .set( 'Authorization', this.getToken() );

    return this._http.get(`${this.url}user/${id}`, { headers });

  }

  // * ------------------------------------------------------------------------------------------------ * //

}
