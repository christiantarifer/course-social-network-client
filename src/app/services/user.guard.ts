import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// ****************************************************************************** //

import { User } from '../models/user';

// ****************************************************************************** //

import { UserService } from './user.service';

// ****************************************************************************** //

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  public identity: User;

  constructor(
    private _router: Router,
    private _userService: UserService
   ){
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      this.identity = this._userService.getIdentity();

      if ( this.identity && ( this.identity.role === 'ROLE_USER' || this.identity.role === 'ROLE_ADMIN' ) ) {

        return true;

      } else {

        this._router.navigate(['/login']);

        return false;

      }


  }




}
