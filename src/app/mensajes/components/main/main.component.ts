import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: [
  ]
})
export class MainComponent implements OnInit {

  public title: string;

  constructor() {
    this.title = 'Mensajes privados';
  }

  ngOnInit(): void {
    console.log('main.component de módulo de mensajes...');
  }

}
