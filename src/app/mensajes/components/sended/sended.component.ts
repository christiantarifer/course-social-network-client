import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// ************************************************************************** //

import { Message } from '../../../models/message';
import { User } from '../../../models/user';

// ************************************************************************** //

import { Global } from '../../../services/global';
import { FollowService } from '../../../services/follow.service';
import { MessageService } from '../../../services/message.service';
import { UserService } from '../../../services/user.service';

// ************************************************************************** //

@Component({
  selector: 'app-sended',
  templateUrl: './sended.component.html',
  styles: [
  ]
})
export class SendedComponent implements OnInit {

  public title: string;
  public identity: User;
  public token;
  public url: string;
  public status: string;
  public follows;
  public messages: Message[];
  public page: number = 1;
  public pages: number;
  public total: number;
  public next_page: number;
  public prev_page: number;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService,
    private _messageService: MessageService
  ) {
    this.title = 'Mensajes Enviados';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = Global.url;
  }

  ngOnInit(): void {

    console.log(' sended.component cargado... ');
    this.actualPage();

  }

  // ****************************************************************************** //

  actualPage() {
    this._route.params.subscribe(params => {

      let page = Number(params.page);

      console.log(page);

      if (!page) { page = 1; }

      this.page = page;

      if (!page) {

        this.page = 1;

      } else {

        this.next_page = page + 1;

        this.prev_page = page - 1;

        if (this.prev_page <= 0) {

          this.prev_page = 1;

        }

      }

      console.log(page);

      this.getMessages( this.token , this.page);

    });

  }

  // ****************************************************************************** //

  getMessages(token, page): void {
    this._messageService.getEmitMessages( token, page).subscribe(

      response => {

        console.log(response);

        if ( !response.messages ){

          console.error( 'something went wrong' );

        } else {

          this.messages = response.messages;
          this.total = response.total;
          this.pages = response.pages;

        }

      },

      error => { console.error(error); }

    );
  }

  // ****************************************************************************** //

}
