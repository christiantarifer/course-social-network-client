import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// ************************************************************************** //

import { Message } from '../../../models/message';
import { Follow } from '../../../models/follow';
import { User } from '../../../models/user';

// ************************************************************************** //

import { Global } from '../../../services/global';
import { FollowService } from '../../../services/follow.service';
import { MessageService } from '../../../services/message.service';
import { UserService } from '../../../services/user.service';

// ************************************************************************** //

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [
  ]
})
export class AddComponent implements OnInit {

  public title: string;
  public message: Message;
  public identity: User;
  public token;
  public url: string;
  public status: string;
  public follows;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService,
    private _messageService: MessageService
  ) {
    this.title = 'Enviar Mensaje';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = Global.url;
    this.message = new Message('', '', '', '', this.identity._id, '');
  }

  ngOnInit(): void {

    console.log('add.component cargado correctamente');
    this.getMyFollows();

  }

  /**
   * getMyFollows: Retrieves all the users that follows the user
   */
  getMyFollows() {

    this._followService.getMyFollows( this.token ).subscribe(

      response => { this.follows = response.follows },

      error => { console.error(error); }

    );

  }

  /**
   * onSubmit Sends the message to the targeted user
   */
  onSubmit(form){
    console.log( this.message );

    this._messageService.addMessage(this.token, this.message).subscribe(

      response => {

        if( response.message ){

          this.status = 'success';
          form.reset();

        }

      },

      error => { 
        console.error( error );
        this.status = 'error';
      }

    );

  }

}
