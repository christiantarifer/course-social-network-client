import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// * MODELS
import { User } from 'src/app/models/user';


// * SERVICES
import { Global } from '../../services/global';
import { UserService } from '../../services/user.service';
import { UploadService } from '../../services/upload.service';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styles: [
  ]
})
export class UserEditComponent implements OnInit {

  public title: string;
  public user: User;
  public identity;
  public token;
  public status: string;
  public fileToUpload: Array<File>;
  public url: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService
  ) {

    this.title = 'Actualizar mis datos';
    this.user = this._userService.getIdentity();
    this.identity = this.user;
    this.token = this._userService.getToken();
    this.url = Global.url;

  }

  ngOnInit(): void {
    console.log(this.user);
    console.log('user-edit.componente se ha cargado ...');

  }

  onSubmit(): void {

    console.log(this.user);

    this._userService.updateUser( this.user ). subscribe(

      response => {

        if ( !response.user ){

          this.status = 'error';

        } else {

          this.status = 'success';

          localStorage.setItem( 'identity', JSON.stringify(this.user) );

          this.identity = this.user;

          // * UPLOAD USER IMAGE
          this._uploadService.makeFileRequest(`${this.url}upload-image-user/${this.user._id}`, [], this.fileToUpload, this.token, 'image' )
                  .then( (result: any) => {
                    console.log( result );
                    this.user.image = result.user.image;
                    localStorage.setItem('identity', JSON.stringify(this.user) );
                  } );

        }

      },

      error => {

        const errorMessage = error;

        console.log( errorMessage );

        if ( errorMessage != null ){

          this.status = 'error';

        }

      }

    );

  }

  fileChangeEvent( fileInput: any ) {

    this.fileToUpload = <Array<File>>fileInput.target.files;

    console.log( this.fileToUpload );

  }

}
