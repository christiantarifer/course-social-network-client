import { Component, OnInit } from '@angular/core';
import {  Router, ActivatedRoute, Params } from '@angular/router';

//  * MODELS
import { User } from '../../models/user';


// * SERVICES
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public title: string;
  public user: User;
  public status: string;
  public identity;
  public token;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService:UserService
  ) {

    this.title = 'Identifícate';
    this.user = new User('', '', '', '', '', '', 'ROLE_USER', '');
   }

  ngOnInit(): void {
    console.log('Login Component');
  }

  onSubmit(loginForm): void {

    // LOG IN THE USER AND GET DATA
    this._userService.signup(this.user).subscribe(
      response => {

        this.identity = response.user;

        console.log(this.identity);

        if ( !this.identity || !this.identity._id) {

          this.status = 'error';


        } else {

          // * PERSIST USER'S DATA
          localStorage.setItem('identity', JSON.stringify(this.identity) );



          // * GET TOKEN
          this.getToken();

        }


      },
      error => {

        const errorMessage = error;

        console.log(errorMessage);

        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }
    );

  }

  getToken(): void {

    // LOG IN THE USER A GET DATA
    this._userService.signup(this.user, 'true').subscribe(
      response => {

        this.token = response.token;

        console.log(this.token);

        if ( this.token.length <= 0 ) {

          this.status = 'error';


        } else {

          // * PERSIST USER'S TOKEN
          localStorage.setItem('token', JSON.stringify(this.token) );

          // * GET COUNTERS OR USER STATISTICS
          this.getCounters();


        }


      },
      error => {

        const errorMessage = error;

        console.log(errorMessage);

        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }
    );

  }

  getCounters(): void {

    this._userService.getCounters().subscribe(

      response => {

        localStorage.setItem('stats', JSON.stringify(response) );

        this.status = 'success';

        this._router.navigate(['/']);

      },

      error => {

        console.log(error);

      }

    );

  }

}
