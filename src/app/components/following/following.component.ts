import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// *************************************************************** //

import { User } from '../../models/user';
import { Follow } from '../../models/follow';

// *************************************************************** //

import { FollowService } from '../../services/follow.service';
import { UserService } from '../../services/user.service';
import { Global } from '../../services/global';

// *************************************************************** //

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styles: [
  ]
})
export class FollowingComponent implements OnInit {

  public title: string;
  public url: string;
  public identity: User;
  public token;
  public page: number;
  public next_page: number;
  public prev_page: number;
  public total: number;
  public pages: number;
  public user: User;
  public users: User[];
  public following;
  public follows;
  public followUserOver;
  public status: string;
  public userPageId;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService
  ) {
    this.url = Global.url;
    this.title = 'Usuarios seguidos por',
      this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit(): void {
    console.log('following.component has been loaded');
    this.actualPage();
  }

  // ***************************************************************** //

  actualPage() {
    this._route.params.subscribe(params => {

      let page = Number(params.page);
      const user_id = params.id;
      this.userPageId = user_id;

      if (!page) { page = 1; }

      this.page = page;

      if (!page) {

        this.page = 1;

      } else {

        this.next_page = page + 1;

        this.prev_page = page - 1;

        if (this.prev_page <= 0) {

          this.prev_page = 1;

        }

      }

      console.log(page);

      this.getUser(user_id, page);

    });
  }

  // ***************************************************************** //

  getFollows(user_id: string, page: number) {

    console.log('Usuario ID: ', user_id);

    this._followService.getFollowing(this.token, user_id, page).subscribe(

      response => {
        console.log(page);
        if (!response.follows) {

          this.status = 'error';

        } else {

          console.log(response);

          console.group('Following Response');
          this.total = Number(response.total);
          console.log('Total : ', this.total);

          this.following = response.follows;

          this.pages = Number(response.pages);
          console.log('Page : ', this.pages);

          this.follows = response.users_following;

          console.log(this.follows);
          console.groupEnd();

          if (page > this.pages) {

            this._router.navigate(['/gente', 1]);

          }

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error(`Error : ${errorMessage}`);

        if (errorMessage != null) {

          this.status = 'error';

        }

      }

    );

  }

  // ***************************************************************** //

  mouseEnter(user_id) {

    this.followUserOver = user_id;

  }

  // ***************************************************************** //

  mouseLeave(user_id) {

    this.followUserOver = 0;

  }

  // ***************************************************************** //

  followUser(followed) {

    const follow = new Follow('', this.identity._id, followed);

    this._followService.addFollow(this.token, follow).subscribe(

      response => {

        if (!response.follow) {

          this.status = 'error';

        } else {

          this.status = 'success';

          this.follows.push(followed);

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error(`Error : ${errorMessage}`);

        if (errorMessage != null) {

          this.status = 'error';

        }

      }

    );

  }

  // ***************************************************************** //

  unfollowUser(followed) {

    this._followService.deleteFollow(this.token, followed).subscribe(

      response => {

        const search = this.follows.indexOf(followed);

        if (search !== -1) {

          this.follows.splice(search, 1);

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error(`Error : ${errorMessage}`);

        if (errorMessage != null) {

          this.status = 'error';

        }

      }


    );

  }

  // ***************************************************************** //

  getUser(user_id, page) {

    this._userService.getUser(user_id).subscribe(

      response => {

        if (response.user) {

          this.user = response.user;
          // RETURN USER LIST
          this.getFollows(user_id, page);

        } else {

          this._router.navigate(['/home'])

        }


      },

      error => {

        const errorMessage = <any>error;

        console.error(`Error : ${errorMessage}`);

        if (errorMessage != null) {

          this.status = 'error';

        }


      }

    )

  }

  // ***************************************************************** //

}
