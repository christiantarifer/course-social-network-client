import { Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';

import {  Router, ActivatedRoute, Params } from '@angular/router';

// ******************************************************************** //

import { User } from '../../models/user';
import { Publication } from '../../models/publication';

// ******************************************************************** //

import { Global } from '../../services/global';
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';
import { UploadService } from '../../services/upload.service';

// ******************************************************************** //

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public identity: User;
  public token;
  public stats;
  public url: string;
  public status;
  public publication: Publication;
  public filesToUpload: Array<File>;
  @ViewChild('pubImg') pubImg: ElementRef;

  // Output
  @Output() sended = new EventEmitter

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationService: PublicationService,
    private _uploadService: UploadService
  ) {

    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.stats = this._userService.getStats();
    this.url = Global.url;
    this.publication = new Publication( '', '', '', '', this.identity._id );
  }

  ngOnInit(): void {

    console.log('sidebar component ha sido cargado');

  }

  // ************************************************************************************************ //

  onSubmit(form, $event): void {

    console.log(this.publication);

    this._publicationService.addPublication(this.token, this.publication).subscribe(

      response => {

        if ( response.publication ){

          // this.publication = response.publication;
          
          if( this.filesToUpload && this.filesToUpload.length ) {

            // Upload image
            this._uploadService.makeFileRequest(this.url+'upload-image-pub/'+response.publication._id, [], this.filesToUpload, this.token, 'image')
                  .then( (result: any) => {

                    this.publication.file = result.image;
                    this.status = 'success';
                    form.reset();
                    this.pubImg.nativeElement.value = '';
                    this._router.navigate(['/timeline']);
                    this.sended.emit({ send: 'true' })

                  })
            
          } else {

            this.status = 'success';
            form.reset();
            this.pubImg.nativeElement.value = '';
            this._router.navigate(['/timeline']);
            this.sended.emit({ send: 'true' })

          }
          

        } else {

          this.status = 'error';
          console.error(response);

        }

      },

      error => {

        const errorMessage = error;

        console.error(errorMessage);
        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }

    );

  }

  // ************************************************************************************************ //

  fileChangeEvent( fileInput: any ){

    this.filesToUpload = <Array<File>>fileInput.target.files; 

  }


  // ************************************************************************************************ //

  sendPublication(event) {

    console.log( event );
    this.sended.emit({ send: 'true' });

  }

  // ************************************************************************************************ //

}
