import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';



// *************************************************************** //

import { User } from '../../models/user';
import { Follow } from '../../models/follow';

// *************************************************************** //

import { FollowService } from '../../services/follow.service';
import { UserService } from '../../services/user.service';
import { Global } from '../../services/global';


// *************************************************************** //

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: [
  ]
})
export class UsersComponent implements OnInit {

  public title: string;
  public url: string;
  public identity: User;
  public token;
  public page: number;
  public next_page: number;
  public prev_page: number;
  public total: number;
  public pages: number;
  public users: User[];
  public follows;
  public followUserOver;
  public status: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService
  ) {
    this.url = Global.url;
    this.title = 'Gente',
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

   }

  ngOnInit(): void {
    console.log('users.component has been loaded');
    this.actualPage();
  }

  // ***************************************************************** //

  actualPage() {
    this._route.params.subscribe( params =>  {

      let page = Number(params.page);

      if ( !page ) { page = 1; }

      this.page = page;

      if ( !page ) {

        this.page = 1;

      } else {

        this.next_page = page + 1;

        this.prev_page = page - 1;

        if ( this.prev_page <= 0 ){

          this.prev_page = 1;

        }

      }

      console.log(page);

      // RETURN USER LIST
      this.getUsers(page);

    });
  }

  // ***************************************************************** //

  getUsers(page: number) {


    this._userService.getUsers(page).subscribe(

      response => {
        console.log(page);
        if ( !response.users ){

          this.status = 'error';

        } else {

          this.total = Number(response.total);
          console.log(this.total);

          this.users = response.users;

          this.pages = Number(response.pages);
          console.log(this.pages);

          this.follows = response.users_following;

          console.log(this.follows);

          if ( page > this.pages ) {

            this._router.navigate(['/gente', 1] );

          }

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error( `Error : ${errorMessage}` );

        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }

     );

  }

  // ***************************************************************** //

  mouseEnter(user_id){

    this.followUserOver = user_id;

  }

  // ***************************************************************** //

  mouseLeave(user_id){

    this.followUserOver = 0;

  }

  // ***************************************************************** //

  followUser( followed ) {

    const follow = new Follow('', this.identity._id, followed);

    this._followService.addFollow( this.token, follow ).subscribe(

      response => {

        if ( !response.follow ) {

          this.status = 'error';

        } else {

          this.status = 'success';

          this.follows.push( followed );

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error( `Error : ${errorMessage}` );

        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }

     );

  }

  // ***************************************************************** //

  unfollowUser( followed ) {

    this._followService.deleteFollow( this.token, followed ).subscribe(

      response => {

        const search = this.follows.indexOf( followed );

        if ( search !== -1) {

          this.follows.splice(search, 1);

        }

      },

      error => {

        const errorMessage = <any>error;

        console.error( `Error : ${errorMessage}` );

        if ( errorMessage != null ) {

          this.status = 'error';

        }

      }


    );

  }

  // ***************************************************************** //

}
