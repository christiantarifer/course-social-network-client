import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  public title: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router
  ) {

    this.title = 'Bienvenido a NGSocial';

   }

  ngOnInit(): void {

    console.log('Home component has been loaded');

  }


}
