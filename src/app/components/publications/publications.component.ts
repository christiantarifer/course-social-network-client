import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// ****************************************************************** //

import { Publication } from '../../models/publication';
import { User } from '../../models/user';

// ****************************************************************** //

import { Global } from '../../services/global';
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';

// ****************************************************************** //

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styles: [
  ]
})
export class PublicationsComponent implements OnInit {

  public identity: User;
  public token;
  public title: string;
  public url: string;
  public status: string;
  public page: number;
  public pages: number;
  public itemsPerPage: number;
  public total: number;
  public noMore = false;
  public publications: Publication[];
  @Input() user: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationService: PublicationService
  ) {
    this.title = 'Publicaciones';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = Global.url;
    this.page = 1;
  }

  ngOnInit(): void {

    this.getPublications( this.user, this.page );

  }


  getPublications(user:string, page: number, adding = false ): void {

    this._publicationService.getPublicationsUser(this.token, user, page).subscribe(

      response => {   

        if ( response.publications ){
          this.page = page;
          this.total = response.total_items;
          this.pages = response.pages;
          this.itemsPerPage = response.items_per_page;
          this.publications = response.publications;

          if ( !adding ) {

            this.publications = response.publications;

          } else {

            console.log( 'Entrando al adding' );
            const arrayA = this.publications;
            console.log( arrayA );
            const arrayB = response.publications;
            console.log( arrayB );

            this.publications = arrayA.concat( arrayB );
            console.log(this.publications);

            $('html, body').animate({ scrollTop: $('html').prop('scrollHeight')}, 500);

          }

          // if ( page > this.pages ) {

          //   this._router.navigate(['/home']);

          // }

        } else {

          this.status = 'error';

        }
      },

      error => {

        const errorMessage = error;
        console.log(errorMessage);

        if ( errorMessage  != null ){

          this.status = 'error';

        }

      }

    );

  }

  viewMore(): void {

    this.page += 1;

    if ( this.page === this.pages ) {
      this.noMore = true;
    }

    this.getPublications( this.user, this.page, true );

  }

}
