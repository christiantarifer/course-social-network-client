import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// ***************************************************************************** //

import { User } from '../../models/user';
import { Follow } from '../../models/follow';

// ***************************************************************************** //

import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { Global } from '../../services/global';

// ***************************************************************************** //

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: [
  ]
})
export class ProfileComponent implements OnInit {

  public title: string;
  public user: User;
  public status: string;
  public identity;
  public token;
  public url;
  public stats;
  public followed: boolean;
  public following: boolean;
  public followUserOver;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService
  ) {

    this.title = 'Perfil';
    this.identity = this._userService.identity;
    this.token = this._userService.token;
    this.url = Global.url;
    this.followed = false;
    this.following = false;

  }

  ngOnInit(): void {

    console.log(' profile.component cargado correctamente ');
    this.loadPage();

  }

  loadPage(): void {

    this._route.params.subscribe( params => {

      const id = params.id;

      this.getUser(id);
      this.getCounters(id);

    });

  }

  // ********************************************************************************************************** //


  getUser(id): void{

    this._userService.getUser(id).subscribe(

      response => {

        if ( response.user ) {

          console.log(response);

          this.user = response.user;

          // * FOLLWING STATUS * //

          if ( response.following && response.following._id ){

            this.following = true;

          } else {

            this.following = false;

          }
          // ********************************* //

          // * FOLLOWED STATUS * //

          if ( response.following && response.followed._id ) {

            this.followed = true;

          } else {

            this.followed = false;

          }

          // ********************************* //

        } else {

          this.status = 'error';

        }

      },

      error => {

        console.log( error );
        this._router.navigate(['/perfil', this.identity._id]);
      }

    );

  }

  // ********************************************************************************************************** //

  getCounters(id): void{

    this._userService.getCounters(id).subscribe(

      response => {

        this.stats = response;

      },

      error => {

        console.log(error);

      }

    );

  }

  // ********************************************************************************************************** //

  followUser( followed ): void {

    let follow = new Follow( '', this.identity._id, followed );

    this._followService.addFollow( this.token, follow ).subscribe(

      response => {

        this.following = true;

      },

      error => {

        console.log(error);

      }

    );

  }

  // ********************************************************************************************************** //

  unfollowUser( followed ): void{

    this._followService.deleteFollow( this.token, followed ).subscribe(

      response => {

        this.following = false;

      },

      error => {

        console.error(error);

      }

    )


  }

  // ********************************************************************************************************** //

  mouseEnter( user_id ){

    this.followUserOver = user_id;

  }

  mouseLeave( ){

    this.followUserOver = 0;

  }

}
