import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// ****************************************************************** //

import { Publication } from '../../models/publication';
import { User } from '../../models/user';

// ****************************************************************** //

import { Global } from '../../services/global';
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';

// ****************************************************************** //
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  public identity: User;
  public token;
  public title: string;
  public url: string;
  public status: string;
  public page: number;
  public pages: number;
  public itemsPerPage: number;
  public total: number;
  public noMore = false;
  public publications: Publication[];
  public showImage: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationService: PublicationService
  ) {
    this.title = 'Timeline';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = Global.url;
    this.page = 1;
   }

  ngOnInit(): void {

    this.getPublications( this.page );

  }

  // ************************************************************************************** //

  getPublications( page: number, adding = false ): void {

    this._publicationService.getPublications(this.token, page).subscribe(

      response => {

        if ( response.publications ){
          this.page = page;
          console.log(this.page);
          this.total = response.total_items;
          this.pages = response.pages;
          this.itemsPerPage = response.items_per_page;
          this.publications = response.publications;

          if ( !adding ) {

            this.publications = response.publications;

          } else {

            console.log( 'Entrando al adding' );
            const arrayA = this.publications;
            const arrayB = response.publications;

            this.publications = arrayA.concat( arrayB );

            $('html, body').animate({ scrollTop: $('body').prop('scrollHeight')}, 500);

          }

          if ( page > this.pages ) {

            this._router.navigate(['/home']);

          }

        } else {

          this.status = 'error';

        }
      },

      error => {

        const errorMessage = error;
        console.log(errorMessage);

        if ( errorMessage  != null ){

          this.status = 'error';

        }

      }

    );

  }

  // ************************************************************************************** //

  viewMore(): void {

    this.page += 1;

    if ( this.page === this.pages ) {
      this.noMore = true;
    }

    this.getPublications( this.page, true );

  }

  // ************************************************************************************** //

  refresh( event = null ): void {

    this.getPublications(1);

  }

  // ************************************************************************************** //

  showThisImage(id) {

    this.showImage = id
    console.log(this.showImage);
  }

  // ************************************************************************************** //

  hideThisImage(id) {
    this.showImage = id;
    this.showImage = 0;
    console.log(this.showImage);

  }

  // ************************************************************************************** //

  deletePublication( id ){

    this._publicationService.deletePublication(this.token, id).subscribe(

      response => {

        this.refresh();

      },

      error => {

        console.error( error )

      }

    );

  }

  // ************************************************************************************** //

}
