import { Component, OnInit } from '@angular/core';
import {  Router, ActivatedRoute, Params } from '@angular/router';

// * MODELS
import { User } from '../../models/user';

// * SERVICES
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public title: string;

  public user: User;

  public status: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {

    this.title = 'Regístrate';
    this.user = new User('', '', '', '', '', '', 'ROLE_USER', '');
  }

  ngOnInit(): void {

    console.log('Register component');

  }

  onSubmit(registerForm): void {

    console.log('********************************************');
    console.log(registerForm.value);
    console.log('********************************************');

    this._userService.register(this.user).subscribe(
      response => {

        if (response.user && response.user._id) {

          console.log( response.user );

          this.status = 'success';

          registerForm.reset();

        } else {

          this.status = 'error';

        }


      },
      error => {

        console.log( <any>error );

      }
    );
  }

}
