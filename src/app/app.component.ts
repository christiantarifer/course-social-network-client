import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//  * MODELS
import { User } from './models/user';

// * Services
import { UserService } from './services/user.service';

// * Url
import { Global } from './services/global';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ UserService ]
})
export class AppComponent implements OnInit, DoCheck {
  public title: string;
  public identity: User;
  public url: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
     ){

    this.title = 'NGSOCIAL';
    this.url = Global.url;

  }

  ngOnInit(): void {

    this.identity = this._userService.getIdentity();

    if ( !this.identity || this.identity === undefined) {

      console.warn('Hi!, please log in to enjoy talking to other people.');

    } else {

      console.log(this.identity);

    }


  }

  ngDoCheck(): void {

    this.identity = this._userService.getIdentity();

  }

  logout(): void {

    localStorage.clear();

    this.identity = null;

    this._router.navigate(['/']);

  }

}
